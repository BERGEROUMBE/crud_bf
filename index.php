<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	
	
</head>
<body class="corps ">

<nav class="navbar navbar-dark bg-dark" role="navigation">
	 <div class="container">
	   <div class="navbar navbar-default ">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="glyphicon glyphicon-list"></span>
			</button>
			<form class="navbar-form pull-right">
				<buttom class="btn btn-primary"><a href="pages/inscription.php">Inscription</a></buttom>
				<buttom class="btn btn-info"><a href="pages/connexion.php">Connexion</a></buttom>
			</form>
		</div>
	</div>
</nav>