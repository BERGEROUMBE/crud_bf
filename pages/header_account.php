<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/style_account.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	
</head>
<style type="text/css">
   
	ul{
		list-style-type: none;
		font-size: 20px;
		font-weight: bold;
	}

	.nom{
		padding-right: 50px;
		margin-top: 50px;
	}
	img{
		border-radius: 50%;
		height: 150px;
		width: 150px;
	}
	@media screen and (max-width: 768px){
		img{
			height: 90px;
			width: 90px;
		}
	}
</style>

<body class="corps ">
	<div class="container">
		<div class="row">
			<nav class="navbar navbar-expand-lg ">
		  		<div class=" navbar-collapse pull pull-right" >
			    
			      	<ul class="navbar-nav">
				      	<li><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
				      	<li class="nav-item dropdown">
				       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				          		<?php echo "<img class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
				        	</a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					        	<div class="dropdown-divider"></div>
					          	<a class="dropdown-item" href="profil_user.php" id="profil">Mon profil</a>
					          	<div class="dropdown-divider"></div>
					          		<a class="dropdown-item" href="deconnexion.php">Déconnexion</a>
					          	<div class="dropdown-divider"></div>
					        </div>
				      	</li>
				    </ul>
			      	
			    
			 	 </div>
			</nav>
		</div>
	</div>


	<script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>