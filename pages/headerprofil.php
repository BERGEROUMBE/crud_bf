<?php 
	session_start();
	if (isset($_SESSION['USE'])) {
		$nom = $_SESSION['USE']['nom'];
		$prenom = $_SESSION['USE']['prenom'];
		$email = $_SESSION['USE']['email'];
		$photo = $_SESSION['USE']['photo'];
	} else {
		$nom = $_SESSION['USER']['nom'];
		$prenom = $_SESSION['USER']['prenom'];
		$email = $_SESSION['USER']['email'];
		$photo = $_SESSION['USER']['photo'];
	  }
 ?>
<!DOCTYPE html>
<html>
<head>
	<title> Mon Profil </title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="../css/headerprofil.css">
	<meta name="viewport" content="width-device-width, initial-scale=1. shrink-to-fit=no">

</head>

<body class="corps">
	<div class="container-fluid">
			<div class="navbar navbar-default Navigation">
		    <div class="navbar-header ">
				   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				   </button>
				   <a class="navbar-brand" href="#"> <span class="My"> B </span> <span  class="Formular"> F </span> </a>
		    </div>
			  <ul class="nav navbar-nav navbar-right text-center Menu">
		        <li style="margin-top: 25px;"> <?php echo $nom." ".$prenom ?> </li>
		        <li style="margin-right: 25px;" class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		          	<?php echo "<img class='img-rounded' src='../images/".$photo." ' style=\"height:40px; width:40px;\">" ?>
		          </a>
		          <ul class="dropdown-menu"style="background-color: inherit;">
		            <li><a href="deconnection.php"> Deconnexion </a></li>
		          </ul>
		        </li>
	      </ul>
			</div>