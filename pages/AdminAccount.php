<?php 
	session_start();
	$bdd= new PDO('mysql:host=localhost;dbname=users','root','', array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
	if(isset($_POST)){
                  if (isset($_FILES['photo']) AND $_FILES['photo']['error'] == 0)
                  {
                      if ($_FILES['photo']['size'] <= 3000000)
                      {
                        $infosfichier = pathinfo($_FILES['photo']['name']);
                        $extension_upload = $infosfichier['extension'];
                        $extensions_autorisees = array('jpg', 'jpeg', 'png','JPEG','PNG','JPG');
                        if (in_array($extension_upload, $extensions_autorisees))
                        {
                          move_uploaded_file($_FILES['photo']['tmp_name'], '../images/' . basename($_FILES['photo']['name']));
                          $p = $_FILES['photo']['name'];
                          $Image="<img style='height:50px; width:50px;' src='../images/".$p." ' >"."<br>";
                        }
                        else
                        {
                            echo 'extention non-autorisee';
                        }
                      }
                      else
                      {
                          echo 'Image trop Volumineuse';
                      }
                  }
              }



 ?>



<!DOCTYPE html>
<html>
<head>
	<title>Administration</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>

<style type="text/css">

	ul{
		list-style-type: none;
		font-size: 20px;
		font-weight: bold;
	}

	.nom{
		padding-right: 50px;
		margin-top: 50px;
	}
	img{
		border-radius: 50%;
		height: 150px;
		width: 150px;
	}
	@media screen and (max-width: 768px){
		img{
			height: 90px;
			width: 90px;
		}
	}
</style>

<body>
	<?php 
	if (isset($_SESSION['USE'])){
		header('location:header_account.php');
	}else{?>
		<div class="container">
				<nav class="navbar navbar-expand-lg ">
			  		<div class=" navbar-collapse pull pull-right" >
				    
				      	<ul class="navbar-nav">
					      	<li><span class="nom"> <?php echo $_SESSION['USER']['nom']." ".$_SESSION['USER']['prenom'] ?></span></li>
					      	<li class="nav-item dropdown">
					       		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          		<?php echo "<img class='profil' src='../images/".$_SESSION['USER']['photo']."'>" ?>
					        	</a>
						        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
						        	<div class="dropdown-divider"></div>
						          	<a class="dropdown-item" href="profil_admin.php">Mon profil</a>
						          	<div class="dropdown-divider"></div>
						          		<a class="dropdown-item" href="deconnexion.php">Déconnexion</a>
						          	<div class="dropdown-divider"></div>
						        </div>
					      	</li>
					    </ul>
				      	
				    
				 	 </div>
				</nav>
		</div>
		<div class="container">
			<div class="row text-center" style="color: #00B59C; font-family: 'Ubuntu', sans-serif; font-weight: bold; font-size: 30px; margin: 25px 0;"> Liste Des Membres Inscrits Sur La Plateforme</div>
			<table class="table table-bordered table-striped table-hover">
				<thead class="thead-light">
					<tr>
						<th class="text-center id"style="width: 30px;"> id </th>
						<th class="text-center nom"style="width: 250px;"> Nom </th>
						<th class="text-center prenom" style="width: 250px;"> Prenom </th>
						<th class="text-center etat" style="width: 80px;"> Etat </th>
						<th class="text-center action" style="width: 90px;"> Action </th>
					</tr>
				</thead>
				<tbody>
					<?php
					$bdd = new PDO('mysql:host=localhost;dbname=users', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
					$response = $bdd->query('SELECT * FROM utilisateur WHERE niveau!=5');
					$i = 0;
					$user = array();
					while ($donnees = $response->fetch()) {
						$user[$i] = $donnees; ?>
							<tr>
								<th ><?php echo $i; ?></th>
								<th ><?php echo $donnees['nom']; ?></th> 
								<th ><?php echo $donnees['prenom']; ?></th> 
								<th >
									<?php if ($donnees['niveau'] == 1) { ?>
										<span style='color:green;font-weight:800;'> actif <span>
									<?php } else if ($donnees['niveau'] == 2) {  ?>
										<span style='color:orange;font-weight:800;'> inactif <span>
									<?php } else { ?>
										<span style='color:red;font-weight:800;'> deleted <span>
									<?php  } ?>
								</th>
								<th> 
									<div style="font-size: 1px;"> 
										<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
											<a href="#">
												<input type="hidden" name="N1"  value="<?php echo "$user[$i][id]"; ?>">
												<input type="hidden" name="N2"  value="consulter">
												<button type="submit" style="background-color:inherit; border: none;" title="consulter">
												  <span class="fa fa-eye " style="margin-left: 10px; color:#00B59C; font-size: 13px;" alt="consulter"></span>
												</button>
											</a>
										</form>
										<?php if($donnees['niveau'] <=2 ){ ?>
										<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
											<a href="#">
												<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
												<input type="hidden" name="N2"  value="supprimer">
												<button  type="submit" style="background-color:inherit; border: none;" title="supprimer">
													<span class="fa fa-trash Supprimer" style="margin-left: 10px;color:red; font-size: 13px;"></span>
												</button>
											</a>
										</form>
										<?php 	} 
										if ($donnees['niveau'] == 1) { ?>
						      		<form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
						      			<a href="#">
						      				<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
						      				<input type="hidden" name="N2"  value="desactiver">
						      				<button  type="submit" style="background-color:inherit; border: none;" title="desactiver">
						      					<span class="glyphicon glyphicon-remove-circle activer_inverse" style="margin-left: 10px;color:orange; font-size: 13px;"></span>
						      				</button>
						      			</a>
						      		</form>
										<?php	} else { ?>
									   <form enctype="multipart/form-data" method="post" action="Management.php" style=" display: inline-block;">
									  		<a href="#">
									  			<input type="hidden" name="N1"  value="<?php echo $user[$i]['id']; ?>">
									  			<input type="hidden" name="N2"  value="activer">
									  			<button  type="submit" style="background-color:inherit; border: none;" title="activer">
									  				<span class="glyphicon glyphicon-ok activer" style="margin-left: 10px;color:green; font-size: 13px;"></span>
									  			</button>
									  		</a>
									   </form>
							      </div>
								</th>
							</tr>
					<?php	}
						$i++;
					}
					?>
				</tbody>
			</table>
	</div>
<?php } ?>

<script type="text/javascript" src="../javascript/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>

