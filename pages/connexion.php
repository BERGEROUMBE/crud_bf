<?php 
	session_start();
	include('header.php');
 ?>


<!-- style de la page -->
<style type="text/css">
	.main {
	    background-color: #FFFFFF;
	    width: 400px;
	    margin-top: 40px;
	    border-radius: 1.5em;
	    box-shadow: 0px 11px 35px 2px rgba(0, 0, 0, 0.14);
	    padding-bottom: 15px;
	}

	.sign {
	    padding-top: 40px;
	    color: #00B59C;
	    font-family: 'Ubuntu', sans-serif;
	    font-weight: bold;
	    font-size: 23px;
	}
	.fb1 span{
		color: black;
	}
	.fb1{
		text-align: center;
	}


	.un {
	    width: 76%;
	    color: rgb(38, 50, 56);
	    font-weight: 700;
	    font-size: 14px;
	    letter-spacing: 1px;
	    background: rgba(136, 126, 126, 0.04);
	    padding: 10px 20px;
	    border: none;
	    border-radius: 20px;
	    outline: none;
	    box-sizing: border-box;
	    border: 2px solid rgba(0, 0, 0, 0.02);
	    margin-bottom: 50px;
	    margin-left: 46px;
	    text-align: center;
	    margin-bottom: 27px;
	    font-family: 'Ubuntu', sans-serif;
	}

	form.formu {
	    padding-top: 40px;
	}

	.pass {
	    width: 76%;
	    color: rgb(38, 50, 56);
	    font-weight: 700;
	    font-size: 14px;
	    letter-spacing: 1px;
	    background: rgba(136, 126, 126, 0.04);
	    padding: 10px 20px;
	    border: none;
	    border-radius: 20px;
	    outline: none;
	    box-sizing: border-box;
	    border: 2px solid rgba(0, 0, 0, 0.02);
	    margin-bottom: 50px;
	    margin-left: 46px;
	    text-align: center;
	    margin-bottom: 27px;
	    font-family: 'Ubuntu', sans-serif;
	}


	.un:focus, .pass:focus {
	    border: 2px solid rgba(255,87,34, .5) !important;
	    
	}
	.mes{
		height: 20px
	}

	.register {
	    border-radius: 5em;
	    color: #fff;
	    background: linear-gradient(to right, #00B59C, #ff5722);
	    border: 0;
	    padding-left: 40px;
	    padding-right: 40px;
	    padding-bottom: 10px;
	    padding-top: 10px;
	    font-family: 'Ubuntu', sans-serif;
	    margin-left: 35%;
	    font-size: 13px;
	    box-shadow: 0 0 20px 1px rgba(0, 0, 0, 0.04);
	}

	.forgot {
	    text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12)!important;
	    color: #ff5722!important;
	    padding-top: 15px!important;
	}

	a .fb2{
	    text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12)!important;
	    color: rgba(255,87,34,0.5)!important;
	    text-decoration: none!important;
	}

	@media (max-width: 600px) {
	    .main {
	        border-radius: 0px;
	}

</style>

<!-- contenu de la page -->


<?php 	
	
	if (isset($_SESSION['USER'])){
		header('location:header_account.php');
	}else{ 

		 ?>
		<div class="container">
			<div class="main col-md-offset-4 col-sm-offset-4 col-xs-offset-4 col-md-4 col-sm-4 col-xs-4">
				<div>
					<p class="sign" align="center">Sign in</p>
				</div>
				 <center class="mes"> 
				 	<?php 
	 					echo "<div style='color:red'> ".$_SESSION['mess']."</div> "; 
	 					session_destroy(); 
 					?> 
 				</center> 
				<div>
					<form class="formu" action="connect_traitement.php" method="POST">
						<input class="un" name="email" type="text"  placeholder="Username or email" required="">
						<input class="pass" name="pwd" type="password" align="center" placeholder="Password" required="">
						<input class="register" type="submit" value="Sing in">     
					</form>
				</div> 
				<div>
					<p class="forgot" align="center"><a class="fb2" href="#">Forgot Password?</a></p>
				</div> 
				<div class="fb1"><span>Don’t have an account?</span> 
					<a href="inscription.php">Sign up</a>
				</div>
				        
			</div>
		</div>
    <?php } ?>

	
 